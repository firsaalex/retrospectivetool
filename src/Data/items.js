const itemsData = [
    {
        id:"1",
        date:"04-08-1991",
        type:"Good",
        author:"Alex",
        text:"This is dummy item 1",
        votes: 0
    },
    {
        id:"2",
        date:"04-08-1991",
        type:"Bad",
        author:"Alex",
        text:"This is dummy item 2",
        votes: 1
    },
    {
        id:"3",
        date:"04-08-1991",
        type:"Ugly",
        author:"Alex",
        text:"This is dummy item 3",
        votes: 1
    },
    {
        id:"4",
        date:"04-08-1991",
        type:"Good",
        author:"Alex",
        text:"This is dummy item 4",
        votes: 1
    },
    {
        id:"5",
        date:"04-08-1991",
        type:"Good",
        author:"Alex",
        text:"This is dummy item 5",
        votes: 1
    },
]

export default itemsData