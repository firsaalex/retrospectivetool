import React from 'react'

const style = {
	width: '100%',
	padding: '25px',
	textAlign: 'center',
	color: '#fff',
	background: 'rgb(9, 95, 234)'
};

function Footer() {
    return (
        <div style = {style}>
		    This is footer. Empty one.
	    </div>
    )
}

export default Footer;